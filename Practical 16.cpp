#include <iostream>
#include <time.h>

int main()
{
    const int mArrSize = 5;
    int mArray[mArrSize][mArrSize];
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int mPrintLine = buf.tm_mday % mArrSize;
    int mSumLine = 0;
    
    for (int i = 0; i < mArrSize; i++) {
        for (int j = 0; j < mArrSize; j++) {
            mArray[i][j] = i + j;
            printf("%d ", mArray[i][j]);
        }
        printf("\n");
    }

    printf("\nPrint sum of line %d\n", mPrintLine);
    for (int i = 0; i < mArrSize; i++) {
        mSumLine += mArray[mPrintLine][i];
    }
    printf("Sum = %d\n", mSumLine);
}
